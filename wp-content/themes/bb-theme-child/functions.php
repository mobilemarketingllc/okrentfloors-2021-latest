<?php

// Defines
define( 'FL_CHILD_THEME_DIR', get_stylesheet_directory() );
define( 'FL_CHILD_THEME_URL', get_stylesheet_directory_uri() );

remove_action('wp_head', 'wp_generator');
// Classes
require_once 'classes/class-fl-child-theme.php';

// Actions
add_action( 'wp_enqueue_scripts', 'FLChildTheme::enqueue_scripts', 1000 );

add_action( 'wp_enqueue_scripts', function(){
    wp_enqueue_script("slick",get_stylesheet_directory_uri()."/resources/slick/slick.min.js","","",1);
    wp_enqueue_script("cookie",get_stylesheet_directory_uri()."/resources/jquery.cookie.min.js","","",1);
	 wp_enqueue_script("script",get_stylesheet_directory_uri()."/script.js","","",11);
    
});

// Register menus
function register_my_menus() {
    register_nav_menus(
        array(
            'footer-1' => __( 'Footer Menu 1' ),
            'footer-2' => __( 'Footer Menu 2' ),
            'footer-3' => __( 'Footer Menu 3' ),
            'footer-4' => __( 'Footer Menu 4' ),
            'footer-5' => __( 'Footer Menu 5' ),
            'site-map' => __( 'Site Map' ),
        )
    );
}
add_action( 'init', 'register_my_menus' );


 
// Enable shortcodes in text widgets
add_filter('widget_text','do_shortcode');

//Facet Title Hook
add_filter( 'facetwp_shortcode_html', function( $output, $atts ) {
    if ( isset( $atts['facet'] ) ) {       
        $output= '<div class="facet-wrap"><strong>'.$atts['title'].'</strong>'. $output .'</div>';
    }
    return $output;
}, 10, 2 );

// Move Yoast to bottom
function yoasttobottom()
{
    return 'low';
}

add_filter('wpseo_metabox_prio', 'yoasttobottom');


function fr_img($id=0,$size="",$url=false,$attr=""){

    //Show a theme image
    if(!is_numeric($id) && is_string($id)){
        $img=get_stylesheet_directory_uri()."/images/".$id;
        if(file_exists(to_path($img))){
            if($url){
                return $img;
            }
            return '<img src="'.$img.'" '.($attr?build_attr($attr):"").'>';
        }
    }

    //If ID is empty get the current post attachment id
    if(!$id){
        $id=get_post_thumbnail_id();
    }

    //If Id is object it means that is a post object, thus retrive the post ID
    if(is_object($id)){
        if(!empty($id->ID)){
            $id=$id->ID;
        }
    }

    //If ID is not an attachment than get the attachment from that post
    if(get_post_type($id)!="attachment"){
        $id=get_post_thumbnail_id($id);
    }

    if($id){
        $image_url=wp_get_attachment_image_url($id,$size);
        if(!$url){
            //If image is a SVG embed the contents so we can change the color dinamically
            if(substr($image_url,-4,4)==".svg"){
                $image_url=str_replace(get_bloginfo("url"),ABSPATH."/",$image_url);
                $data=file_get_contents($image_url);
                echo strstr($data,"<svg ");
            }else{
                return wp_get_attachment_image($id,$size,0,$attr);
            }
        }else if($url){
            return $image_url;
        }
    }
}

if(@$_GET['keyword'] != '' && @$_GET['brand'] !="")
 {
        $url = "https://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
        $url = explode('?' , $url);
  	    setcookie('keyword' , $_GET['keyword']);
	    setcookie('brand' , $_GET['brand']);
	    wp_redirect( $url[0] );
	    exit;
 } 
 else if(@$_GET['brand'] !="" && @$_GET['keyword'] == '' )
 {
        $url = "https://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
        $url = explode('?' , $url);
        setcookie('brand' , $_GET['brand']);
        wp_redirect( $url[0] );
        exit;
 }
 else if(@$_GET['brand'] =="" && @$_GET['keyword'] != '' )
 {
        $url = "https://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
        $url = explode('?' , $url);
        setcookie('keyword' , $_GET['keyword']);
        wp_redirect( $url[0] );
        exit;
 }
 
 
// shortcode to show H1 google keyword fields
function new_google_keyword() 
{
      
   if( @$_COOKIE['keyword'] ==""  && @$_COOKIE['brand'] == "")
   {
        // return $google_keyword = '<h1 class="googlekeyword">Save up to $500 on Flooring<h1>';
        return $google_keyword = '<h1 class="googlekeyword">Save up to $500 on Flooring *<h1>';
   }
   else
   {
       $keyword = $_COOKIE['keyword'];
	   $brand = $_COOKIE['brand'];
    //    return $google_keyword = '<h1 class="googlekeyword">Save up to $500 on '.$brand.' '.$keyword.'<h1>';
    return $google_keyword = '<h1 class="googlekeyword">Save up to $500 on '.$brand.' '.$keyword.'<h1>';
   }
}
  add_shortcode('google_keyword_code', 'new_google_keyword');
  add_action('wp_head','cookie_gravityform_js');

  function cookie_gravityform_js()
  { // break out of php 
  ?>
  <script>
	  var brand_val ='<?php echo $_COOKIE['brand'];?>';
	  var keyword_val = '<?php echo $_COOKIE['keyword'];?>';  

      jQuery(document).ready(function($) {
      jQuery("#input_14_9").val(keyword_val);
      jQuery("#input_14_10").val(brand_val);
    });
  </script>
  <?php  
     setcookie('keyword' , '',-3600); 
     setcookie('brand' , '',-3600);
  
}

// Action to for styling H1 tag - google keyword fields
add_action('wp_head', 'add_css_head');
function add_css_head() {
    
   ?>
      <style>
          .googlekeyword {
             text-align:center;
             color: #fff;
             text-transform: capitalize;   
              /* font-size:2.5em !important; */
              font-size: 36px !important;
           }
      </style>  
   <?php 
}

//check out flooring FUnctionality

add_action( 'wp_ajax_nopriv_check_out_flooring', 'check_out_flooring' );
add_action( 'wp_ajax_check_out_flooring', 'check_out_flooring' );

function check_out_flooring() {

    $arg = array();
    if($_POST['product']=='carpeting'){

        $posttype = array('carpeting');

    }elseif($_POST['product']=='hardwood_catalog,laminate_catalog,luxury_vinyl_tile'){

        $posttype = array('hardwood_catalog','laminate_catalog','luxury_vinyl_tile');

    }elseif($_POST['product']=='tile_catalog' || $_POST['product']=='tile'){

        // $posttype = array('tile_catalog','hardwood_catalog','laminate_catalog','luxury_vinyl_tile','carpeting');
        $posttype = array('tile_catalog');
    }

    $parameters =  explode(",",$_POST['imp_thing']);

foreach($parameters as $para){
     if($para == 'hypoallergenic'){

        $arg['hypoallergenic'] = array(
            'post_type'      => $posttype,
            'post_status'    => 'publish',
            'orderby'        => 'rand',
            'posts_per_page' => 50
        );
     }elseif($para == 'diy_friendly'){

        $arg['diy_friendly'] = array(
            'post_type'      => $posttype,
            'post_status'    => 'publish',
            'orderby'        => 'rand',
            'posts_per_page' => 50,
            'meta_query'     => array(
                'relation' => 'OR',
                array(
                    'key'     => 'installation_facet',
                    'value'   => 'Locking',
                    'compare' => '=',
                    
                ),
                array(
                    'key'     => 'installation_method',
                    'value'   => 'Locking',
                    'compare' => '=',
                    
                ),
            ),             
        );
     }elseif($para == 'aesthetic_beauty'){

        $arg['aesthetic_beauty'] = array(
            'post_type'      => $posttype,
            'post_status'    => 'publish',
            'orderby'        => 'rand',
            'posts_per_page' => 50,
            'meta_query'     => array(
                'relation' => 'OR',
                array(
                    'key'     => 'brand',
                    'value'   => 'Anderson Tuftex',
                    'compare' => '=',
                    
                ),
                array(
                    'key'     => 'brand',
                    'value'   => 'Karastan',
                    'compare' => '=',
                    
                ),
            ),
        );
     }elseif($para == 'eco_friendly'){
        $arg['eco_friendly'] = array(
            'post_type'      => $posttype,
            'post_status'    => 'publish',
            'orderby'        => 'rand',
            'posts_per_page' => 50,
            'meta_query'     => array(
                array(
                    'key'     => 'lifestyle',
                    'value'   => 'Eco',
                    'compare' => 'LIKE',
                    
                ),
            ),
        );
     }
     elseif($para == 'pet_friendly'){

        $arg['pet_friendly'] = array(
            'post_type'      => $posttype,
            'post_status'    => 'publish',
            'orderby'        => 'rand',
            'posts_per_page' => 50,
            'meta_query'     => array(
                'relation' => 'OR',
                array(
                    'key'     => 'lifestyle',
                    'value'   => 'Pet',
                    'compare' => 'LIKE',
                    
                ),
            ),
        );

        
     }elseif($para == 'easy_to_clean'){
        
        $arg['easy_to_clean']=array(
            'post_type'      => $posttype,
            'post_status'    => 'publish',
            'orderby'        => 'rand',
            'posts_per_page' => 50,
            'meta_query'     => array(
                'relation' => 'OR',
                array(
                    'key'     => 'fiber',
                    'value'   => 'SmartStrand',
                    'compare' => 'Like',
                    
                ),
                array(
                    'key'     => 'fiber',
                    'value'   => 'R2x',
                    'compare' => 'Like',
                    
                ),
            ),
        );

        
     }elseif($para == 'durability'){
        
        $arg['durability']=array(
            'post_type'      => $posttype,
            'post_status'    => 'publish',
            'orderby'        => 'rand',
            'posts_per_page' => 50,
            'meta_query'     => array(
                'relation' => 'OR',
                array(
                    'key'     => 'fiber',
                    'value'   => 'SmartStrand',
                    'compare' => 'Like',
                    
                ),
                array(
                    'key'     => 'fiber',
                    'value'   => 'R2x',
                    'compare' => 'Like',
                    
                ),
                array(
                    'key'     => 'fiber',
                    'value'   => 'Stainmaster',
                    'compare' => 'Like',
                    
                ),
            ),
        );

        
     }
     elseif($para == ''){
        
        $arg['colors']=array(
            'post_type'      => array( 'luxury_vinyl_tile', 'laminate_catalog','hardwood_catalog','carpeting','tile' ),
            'post_status'    => 'publish',
            'orderby'        => 'rand',
            'posts_per_page' => 50,
        );

        
     }
     elseif($para == 'stain_resistant'){
        
        $arg['stain_resistant'] =array(
            'post_type'      => $posttype,
            'post_status'    => 'publish',
            'orderby'        => 'rand',
            'posts_per_page' => 50,
            'meta_query'     => array(
                'relation' => 'OR',
                array(
                    'key'     => 'lifestyle',
                    'value'   => 'Stain Resistant',
                    'compare' => 'Like',
                    
                ),
                array(
                    'key'     => 'fiber',
                    'value'   => 'R2x',
                    'compare' => 'Like',
                    
                ),
                array(
                    'key'     => 'fiber',
                    'value'   => 'Stainmaster',
                    'compare' => 'Like',
                    
                ),
                array(
                    'key'     => 'fiber',
                    'value'   => 'SmartStrand',
                    'compare' => 'Like',
                    
                ),
                array(
                    'key'     => 'Collection',
                    'value'   => 'Bellera',
                    'compare' => 'Like',
                    
                ),
            ),
        );

        
     }elseif($para == 'budget_friendly'){
        
        $arg['budget_friendly'] = array(
            'post_type'      => $posttype,
            'post_status'    => 'publish',
            'orderby'        => 'rand',
            'posts_per_page' => 50,
            'meta_query'     => array(
                array(
                    'key'     => 'fiber',
                    'value'   => array( 'SmartStrand','Stainmaster','Bellera','Pet R2x' ),
                    'compare' => 'IN',
                    
                ),
            ),
        );

        
     }elseif($para == 'color'){
        
        $arg['color'] = array(
            'post_type'      => $posttype,
            'post_status'    => 'publish',
            'orderby'        => 'rand',
            'posts_per_page' => 50            
        );

        
     }

}
 
     $i = 1;

     $find_posts = array();
     foreach($parameters as $para){
        $wp_query[$i] = new WP_Query($arg[$para]);
        
        
        $find_posts = array_merge( $find_posts , $wp_query[$i]->posts);        

        $i++;
     }
     
     //print_r($find_posts);
     if (!$find_posts ) {
       
            $args =array(
                'post_type'      => $posttype,
                'post_status'    => 'publish',
                'posts_per_page' => 50,
                'orderby'        => 'rand'
            );
            $find_posts   = new WP_Query($args);
           // var_dump($args , $find_posts);
            $find_posts = $find_posts->posts;
     }

     if ( $find_posts ) {
          
        $content ="";

        foreach ( $find_posts as $post ) {

            $image = swatch_image_product_thumbnail($post->ID,'222','222');
            $sku = get_field( "sku", $post->ID );
           
     $content .= '<div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 singlePro ">
                        <div class="card">
                            <div class="productImgWrap">
                                <img src="'.$image.'" alt="Product Image" style="width:100%!important">
                            </div>
                            <a href="javascript:void(0)" class="closebtn"  >&times;</a>
                            <div class="pro_title">
                                <h4>'.get_field( "collection", $post->ID ).'</h4> 
                                <h3>'.get_field( "color", $post->ID ).'</h3>
                                <p class="brand"><small><i>'.get_field( "brand", $post->ID ).'</i></small></p>
                                <!-- <a class="orderSamplebtn fl-button btnAddAction cart-action"  href="javascript:void(0)">
                                Order sample
                                </a> -->
                                <a class="prodLink" href="'.get_permalink($post->ID).'">See info</a>
                            </div>
                            
                        </div>
                    </div>';
        }

        $content .='<div class="text-center buttonWrap" data-search="'.$_POST['imp_thing'].'">
                    <a class="button" href="javascript:void(0)">view all results</a>
                    <a class="restartQuiz prodLink" href="javascript:void(0)" onclick="location.reload();">Restart Quiz</a>
                </div>';

    }

    echo $content;
    wp_die();
}



  //Yoast SEO Breadcrumb link - Changes for PDP pages
add_filter( 'wpseo_breadcrumb_links', 'wpse_override_yoast_breadcrumb_trail',90 );

function wpse_override_yoast_breadcrumb_trail( $links ) {

    if (is_singular( 'tile_catalog' )) {

        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/',
            'text' => 'Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/tile-and-stone/',
            'text' => 'Tile & Stone',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/tile-and-stone/products/',
            'text' => 'Products',
        );
        array_splice( $links, 1, -1, $breadcrumb );
        
    }
    
    return $links;
}